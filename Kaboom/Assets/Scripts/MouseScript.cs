﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScript : MonoBehaviour
{
    [Tooltip("Mínima X durante el desplazamiento")]
    public float xMin = -10f;
    [Tooltip("Máxima X durante el desplazamiento")]
    public float xMax = 10f;

    private float accMouseMovement = 0.0f;

    private Rigidbody rb;
    
    public Animator anim;
    Vector3 lastPos;
    Vector3 currentPos;

    
    // Start is called before the first frame update
    void Start()
    {
        lastPos = transform.position;
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            enabled = false;
        }
    }
    void Update() 
    {
        accMouseMovement += Input.GetAxisRaw("Mouse X");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        currentPos = transform.position;
        currentPos.x += accMouseMovement;
        accMouseMovement = 0.0f;
        
        if (currentPos == lastPos)
        {
            anim.SetFloat("speed", 0);
        }
        else
        {
            anim.SetFloat("speed", 1);
        }

        lastPos = transform.position;

        if (currentPos.x < xMin)
        {
            currentPos.x = xMin;
        }
        else if (currentPos.x > xMax)
        {
            currentPos.x = xMax;
        }
        transform.position = currentPos;
        
        rb.MovePosition(currentPos);
    }
}