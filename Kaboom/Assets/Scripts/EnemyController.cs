﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour
{
    [Tooltip("Bomba que se lanza")]
    public GameObject bomb;
    [Tooltip("Punto donde se generan las bombas")]
    public Transform spawnPosition;
    [HideInInspector]
    public float bombRate = 0.5f;
    [HideInInspector]
    public float bombSpeed = 3.0f;
    [HideInInspector]
    public float speed = 3.0f;
    [Tooltip("Mínima X durante el desplazamiento")]
    public float xMin = -10f;
    [Tooltip("Máxima X durante el desplazamiento")]
    public float xMax = 10f;
    [HideInInspector]
    public uint bombCount = 1;

    private float currentDir; // 1 / -1 -> izquierda o derecha
    private float nextBombTime;
    private uint remainingBombs; //bombas que faltan x lanzarse



    // Start is called before the first frame update
    void Start()
    {
        //StartBombing();
    }
    public void StartBombing() 
    {
        if (remainingBombs !=0)
        {
            Debug.LogError("StartBombing():..." + remainingBombs);
        }
        else
        {
            if (bombRate <= 0.0)
            {
                Debug.LogWarning("bombRate debe ser positivo");
                bombRate = 1.0f;
            }
            if (bombCount==0)
            {
                Debug.LogWarning("nombCount es: " + bombCount);
                enabled = false;
                return;
            }
            remainingBombs = bombCount;
            currentDir = 1.0f;
            //randomlyChangeDirection();
            nextBombTime = Time.time;
            enabled = true;

        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }

        Vector3 p = transform.position;
        p.x = p.x +  (speed * currentDir * Time.deltaTime);
        if (p.x < xMin)
        {
            p.x = xMin;
            currentDir = currentDir * -1; // currentDir *= -1;
        } else if (p.x > xMax)
        {
            p.x = xMax;
            currentDir = currentDir * -1; // currentDir *= -1;
        }

        transform.position = p;
        if (Time.time >= nextBombTime)
        {
            if (remainingBombs==0)
            {
                enabled = false;
                return;
            }
            else
            {
                Bombard();
                remainingBombs--;
                randomlyChangeDirection();
                nextBombTime = nextBombTime + (1.0f / bombRate); // nextBombTime += 1.0f / bombRate;
            }
        }
    }

    void Bombard()
    {
        if(bomb != null && spawnPosition != null)
        {
            GameObject newBomb;
            newBomb = Instantiate(bomb, spawnPosition.position, spawnPosition.rotation);
            BombController bc;
            bc = newBomb.GetComponent<BombController>();
            bc.speed = bombSpeed;
        }
    }

    void randomlyChangeDirection()
    {
        if (Random.value < 0.5)
        {
            currentDir *= -1;
        }
    }

    public void StopBombing() 
    {
        remainingBombs = 0;
        enabled = false;
    }
}